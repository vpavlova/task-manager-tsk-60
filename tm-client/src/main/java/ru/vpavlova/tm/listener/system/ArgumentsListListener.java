package ru.vpavlova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;

@Component
public class ArgumentsListListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.arg() != null) {
                System.out.println("[" + listener.arg() + "] - " + listener.description());
            }
        }
    }
}
