package ru.vpavlova.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.vpavlova.tm")
public class LoggerConfiguration {

}
